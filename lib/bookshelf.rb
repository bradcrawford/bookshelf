require "active_support/all"
require "digest/md5"
require "erb"
require "logger"
require "nokogiri"
require "notifier"
require "open3"
require "optparse"
require "ostruct"
require "tempfile"
require "pathname"
require "thor"
require "thor/group"
require "yaml"
require "cgi"
require "maruku"

Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"

module Bookshelf
  ROOT = Pathname.new(File.dirname(__FILE__) + "/..")

  autoload :Version,    "bookshelf/version"
  autoload :Generator,  "bookshelf/generator"
  autoload :Cli,        "bookshelf/cli"
  autoload :Markdown,   "bookshelf/adapters/markdown"
  autoload :Parser,     "bookshelf/parser"
  autoload :Exporter,   "bookshelf/exporter"
  autoload :Dependency, "bookshelf/dependency"
  autoload :Stats,      "bookshelf/stats"

  def self.config
    path = Pathname.new("config/config.yml")
    content = File.read(path)
    erb = ERB.new(content).result
    YAML.load(erb).with_indifferent_access
  end

  def self.logger
    @logger ||= Logger.new(File.open("/tmp/bookshelf.log", "a"))
  end

  def self.root_dir
    @root_dir ||= Pathname.new(Dir.pwd)
  end

  def self.render_template(file, locals = {})
    ERB.new(File.read(file)).result OpenStruct.new(locals).instance_eval{ binding }
  end
end
