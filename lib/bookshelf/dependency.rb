module Bookshelf
  class Dependency
    def self.prince?
      @prince ||= `which prince` && $?.success?
    end
  end
end
