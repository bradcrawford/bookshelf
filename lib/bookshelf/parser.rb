require 'open3'

module Bookshelf
  module Parser
    autoload :HTML  , "bookshelf/parser/html"
    autoload :PDF   , "bookshelf/parser/pdf"
    autoload :Epub  , "bookshelf/parser/epub"

    class Base
      # The e-book directory.
      #
      attr_accessor :book_dir

      def self.parse(book_dir)
        new(book_dir).parse
      end

      def initialize(book_dir)
        @book_dir = Pathname.new(book_dir)
      end

      # Return directory's basename.
      #
      def name
        File.basename(Bookshelf.root_dir)
      end

      # Return the configuration file.
      #
      def config
        Bookshelf.config
      end

      def spawn_command(cmd)
        begin
          stdout_and_stderr, status = Open3.capture2e(*cmd)
        rescue Errno::ENOENT => e
          puts e.message
        else
          puts stdout_and_stderr unless status.success?
          status.success?
        end
      end
    end
  end
end
