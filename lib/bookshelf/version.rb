module Bookshelf
  module Version
    MAJOR = 1
    MINOR = 2
    PATCH = 4
    STRING = "#{MAJOR}.#{MINOR}.#{PATCH}"
  end
end
