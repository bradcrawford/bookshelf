# -*- encoding: utf-8 -*-
module Bookshelf
  class Cli < Thor
    FORMATS = %w[pdf html epub]
    check_unknown_options!

    def self.exit_on_failure?
      true
    end

    def initialize(args = [], options = {}, config = {})
      if (config[:current_task] || config[:current_command]).name == "new" && args.empty?
        raise Error, "The e-Book path is required. For details run: bookshelf help new"
      end

      super
    end

    desc "new EBOOK_PATH", "Generate a new e-book structure"

    def new(path)
      generator = Generator.new
      generator.destination_root = path
      generator.invoke_all
    end

    desc "export [OPTIONS]", "Export e-book"
    method_option :only, :type => :string, :desc => "Can be one of: #{FORMATS.join(", ")}"

    def export
      if options[:only] && !FORMATS.include?(options[:only])
        raise Error, "The --only option need to be one of: #{FORMATS.join(", ")}"
      end

      book_dir = Pathname.new("text")
      Bookshelf::Exporter.run(book_dir, options)
    end

    desc "version", "Prints the Bookshelf's version information"
    map %w(-v --version) => :version

    def version
      say "Bookshelf version #{Version::STRING}"
    end

    desc "check", "Check if all external dependencies are installed"

    def check
      result = []

      result << {
        :description => "Prince XML: Converts HTML files into PDF files.",
        :installed => Bookshelf::Dependency.prince?
      }

      result.each do |result|
        text = color(result[:name], :blue)
        text << "\n" << result[:description]
        text << "\n" << (result[:installed] ? color("Installed.", :green) : color("Not installed.", :red))
        text << "\n"

        say(text)
      end
    end

    desc "stats", "Display some stats about your e-book"
    def stats
      stats = Bookshelf::Stats.new(Bookshelf.root_dir)

      say [
        "Chapters: #{stats.chapters}",
        "Words: #{stats.words}",
        "Images: #{stats.images}",
        "Links: #{stats.links}"
      ].join("\n")
    end

    private

    def color(text, color)
      color? ? shell.set_color(text, color) : text
    end

    def color?
      shell.instance_of?(Thor::Shell::Color)
    end
  end
end
