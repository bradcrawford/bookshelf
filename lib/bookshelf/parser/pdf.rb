module Bookshelf
  module Parser
    class PDF < Base
      def parse
        spawn_command ["prince", html_file.to_s, "-o", pdf_file.to_s]
      end
      
      def html_file
        Bookshelf.root_dir.join("output/#{name}.html")
      end

      def pdf_file
        Bookshelf.root_dir.join("output/#{name}.pdf")
      end
    end
  end
end
