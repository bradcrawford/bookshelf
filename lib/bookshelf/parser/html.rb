module Bookshelf
  module Parser
    class HTML < Base
      # List of directories that should be skipped.
      #
      IGNORE_DIR = %w[. .. .svn]

      # Files that should be skipped.
      #
      IGNORE_FILES = /^(TOC)\..*?$/

      # List of recognized extensions.
      #
      EXTENSIONS = %w[md mkdn markdown html]

      # Parse all files and save the parsed content
      # to <tt>output/book_name.html</tt>.
      #
      def parse
        File.open(Bookshelf.root_dir.join("output/#{name}.html"), "w") do |file|
          locals = config.merge({
            :content => content,
            :toc => toc
          })
          file << Bookshelf.render_template(Bookshelf.root_dir.join("templates/html/layout.erb"), locals)
        end
        true
      rescue Exception
        false
      end

      # Return all chapters wrapped in a <tt>div.chapter</tt> tag.
      #
      def content
        @content ||= String.new.tap do |chapters|
          entries.each do |entry|
            chapters << %[<div class="chapter">#{render_file(entry)}</div>]
          end
        end
      end

      def toc
        if @toc.blank?
          @toc = ""
          Nokogiri::HTML(content).css(".chapter h2:first-of-type").each do |xml|
            @toc << %[<div><a href="##{xml.attribute("id")}"><span>#{CGI.escape_html(xml.text)}</span></a></div>]
          end
        end
        return @toc
      end

      private

      # Return a list of all recognized files.
      #
      def entries
        Dir.entries(book_dir).sort.inject([]) do |buffer, entry|
          buffer << book_dir.join(entry) if valid_entry?(entry)
          buffer
        end
      end

      # Check if path is a valid entry.
      # Files that start with a dot or underscore will be skipped.
      #
      def valid_entry?(entry)
        entry !~ /^(\.|_)/ && valid_file?(entry)
      end

      # Check if path is a valid file.
      #
      def valid_file?(entry)
        ext = File.extname(entry).gsub(/\./, "").downcase
        File.file?(book_dir.join(entry)) && EXTENSIONS.include?(ext) && entry !~ IGNORE_FILES
      end

      # Render +file+ considering its extension.
      #
      def render_file(file)
        file_format = format(file)
        content = File.read(file)
        content = case file_format
        when :markdown
          Markdown.to_html(content)
        else
          content
        end
      end

      def format(file)
        case File.extname(file).downcase
        when ".markdown", ".mkdn", ".md"
          :markdown
        else
          :html
        end
      end
    end
  end
end
