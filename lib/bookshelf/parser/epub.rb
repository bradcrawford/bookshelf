module Bookshelf
  module Parser
    class Epub < Base
      def html
        @html ||= Nokogiri::HTML(html_path.read)
      end

      def chapters
        @chapters ||= []
      end

      def assets
        @assets ||= []
      end

      def parse
        _create_directories
        _create_container_xml
        _create_cover_html
        _create_chapter_html
        _create_toc_html
        _create_toc_ncx
        _create_assets
        _create_content_opf
        _create_epub
        true
      rescue Exception
        p $!, $@
        false
      end

      def _create_directories
        FileUtils.rm_rf(tmp_path)
        FileUtils.mkdir_p(tmp_path)
        FileUtils.mkdir_p(File.join(tmp_path, "META-INF"))
        FileUtils.mkdir_p(File.join(tmp_path, "styles"))
        FileUtils.mkdir_p(File.join(tmp_path, "fonts"))
        FileUtils.mkdir_p(File.join(tmp_path, "images"))
      end

      def _create_container_xml
        builder = Nokogiri::XML::Builder.new("encoding" => "utf-8") do |xml|
          xml.container("xmlns" => "urn:oasis:names:tc:opendocument:xmlns:container", "version" => "1.0") {
            xml.rootfiles {
              xml.rootfile "full-path" => "content.opf", "media-type" => "application/oebps-package+xml"
            }
          }
        end
        File.open(File.join(tmp_path, "META-INF", "container.xml"), "w") do |f|
          f.write(builder.to_xml)
        end
      end

      def _create_cover_html
        File.open(File.join(tmp_path, "cover.html"), "w") do |f|
          f.write(Bookshelf.render_template(cover_template_path, :title => config[:title], :authors => config[:authors]))
        end
      end

      def _create_chapter_html
        html.css("div.chapter").each_with_index.map do |chapter, index|
          filename = "chapter_#{index}.html"
          File.open(File.join(tmp_path, filename), "w") do |f|
            f.write(Bookshelf.render_template(chapter_template_path, :content => chapter.inner_html))
          end

          chapters << {
            :text => chapter.css("h2:first-of-type").text,
            :src => filename
          }
        end
      end

      def _create_toc_html
        toc = chapters.map { |chapter|  [chapter[:text], chapter[:src]] }
        File.open(File.join(tmp_path, "toc.html"), "w") do |f|
          f.write(Bookshelf.render_template(toc_template_path, :toc => toc))
        end
      end

      def _create_toc_ncx
        # toc.ncx
        builder = Nokogiri::XML::Builder.new do |xml|
          xml.doc.create_internal_subset(
            "html",
            "-//W3C//DTD XHTML 1.1//EN",
            "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
          )
          xml.ncx("xmlns" => "http://www.daisy.org/z3986/2005/ncx/", "version" => "2005-1") {
            xml.head {
              xml.meta("name" => "dtb:uid", "content" => config[:identifier][:id])
              xml.meta("name" => "dtb:depth", "content" => "1")
              xml.meta("name" => "dtb:totalPageCount", "content" => "0")
              xml.meta("name" => "dtb:maxPageNumber", "content" => "0")
            }
            xml.docTitle {
              xml.text_ config["title"]
            }
            xml.navMap {
              chapters.each_with_index do |chapter, index|
                xml.navPoint("id" => "navpoint-#{index}", "playOrder" => "#{index}") {
                  xml.navLabel {
                    xml.text_ chapter[:text]
                  }
                  xml.content("src" => chapter[:src])
                }
              end
            }
          }
        end
        File.open(File.join(tmp_path, "toc.ncx"), "w") do |f|
          f.write builder.to_xml
        end
      end

      def _create_assets
        base_assets = []
        base_assets << asset_path.join("styles/epub.css").to_s
        base_assets.concat( Dir[asset_path.join("fonts/*.*")])
        base_assets.concat(Dir[asset_path.join("images/*.{jpg,png,gif}")])
        base_assets.each do |base_asset|
          asset = base_asset.sub("#{asset_path.to_s}/", "")
          FileUtils.cp(base_asset, File.join(tmp_path, asset))
          assets << asset
        end
      end

      def _create_content_opf
        builder = Nokogiri::XML::Builder.new do |xml|
          xml.package("xmlns" => "http://www.idpf.org/2007/opf", "unique-identifier" => config[:uid], "version" => 2.0 ) {
            xml.metadata("xmlns:dc" => "http://purl.org/dc/elements/1.1/", "xmlns:dcterms" => "http://purl.org/dc/terms/", "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance", "xmlns:opf" => "http://www.idpf.org/2007/opf") {
              xml["dc"].identifier config[:identifier][:id], {"opf:scheme" => config[:identifier][:type], "id" => config[:uid]}
              xml["dc"].title config[:title]
              xml["dc"].language config[:language]
              xml["dc"].creator config[:authors].join(",")
              xml["dc"].publisher config[:publisher]
              xml["dc"].date config[:published_at]
              xml.meta("name" => "cover", "content" => "cover")
            }
            xml.manifest {
              chapters.each do |chapter|
                xml.item("id" => chapter[:src], "href" => chapter[:src], "media-type" => "application/xhtml+xml")
              end
              assets.each do |asset|
                id = asset.sub(/(styles|fonts|images)\//, "")
                media_type = if asset.ends_with?("css")
                  "text/css"
                else
                  ""
                end
                xml.item("id" => id, "href" => asset, "media-type" => media_type)
              end
              xml.item("id" => "cover", "href" => "cover.html", "media-type" => "application/xhtml+xml")
              xml.item("id" => "toc", "href" => "toc.html", "media-type" => "application/xhtml+xml")
              xml.item("id" => "ncx", "href" => "toc.ncx", "media-type" => "application/x-dtbncx+xml")
            }
            xml.spine("toc" => "ncx") {
              xml.itemref("idref" => "cover")
              xml.itemref("idref" => "toc")
              chapters.each do |chapter|
                xml.itemref("idref" => chapter[:src])
              end
            }
            xml.guide {
              xml.reference("type" => "toc", "title" => "Table of Contents", "href" => "toc.html")
              if chapters.length > 0
                xml.reference("type" => "text", "title" => chapters[0][:text], "href" => chapters[0][:src])
              end
            }
          }
        end
        File.open(File.join(tmp_path, "content.opf"), "w") do |f|
          f.write builder.to_xml
        end
      end

      def _create_epub
        require "zip"
        # mimetype needs to be uncompressed
        Zip::OutputStream::open(epub_path) do |os|
          os.put_next_entry("mimetype", nil, nil, Zip::Entry::STORED, Zlib::NO_COMPRESSION)
          os << "application/epub+zip"
        end
        zipfile = Zip::File.open(epub_path)
        Dir.glob(File.join(tmp_path, "**/*")).each do |path|
          zipfile.add(path.sub("#{tmp_path.to_s}/", ""), path )
        end
        zipfile.commit
      end

      def cover_template_path
        Bookshelf.root_dir.join("templates/epub/cover.erb")
      end

      def toc_template_path
        Bookshelf.root_dir.join("templates/epub/toc.erb")
      end

      def chapter_template_path
        Bookshelf.root_dir.join("templates/epub/page.erb")
      end

      def asset_path
        Bookshelf.root_dir.join("output/assets")
      end

      def html_path
        Bookshelf.root_dir.join("output/#{name}.html")
      end

      def epub_path
        Bookshelf.root_dir.join("output/#{name}.epub")
      end

      def tmp_path
        Bookshelf.root_dir.join("output/tmp")
      end
    end
  end
end
