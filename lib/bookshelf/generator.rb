module Bookshelf
  # The Bookshelf::Generator class will create a new book structure.
  #
  #   ebook = Bookshelf::Generator.new
  #   ebook.destination_root = "/some/path/book-name"
  #   ebook.invoke_all
  #
  class Generator < Thor::Group
    include Thor::Actions

    desc "Generate a new e-Book structure"

    def self.source_root
      File.dirname(__FILE__) + "/../../templates"
    end

    def copy_top_level
      copy_file "Gemfile", "Gemfile"
      copy_file "README.md", "README.md"
    end

    def copy_assets
      copy_file "epub.scss", "assets/styles/epub.scss"
      copy_file "html.scss", "assets/styles/html.scss"
      copy_file "_fonts.scss", "assets/styles/_fonts.scss"
      empty_directory "assets/fonts"
      empty_directory "assets/images"
    end

    def copy_config_file
      template "config.erb", "config/config.yml"
    end

    def copy_helper_file
      copy_file "helper.rb", "config/helper.rb"
    end

    def copy_templates
      copy_file "layout.erb", "templates/html/layout.erb"
      copy_file "cover.erb", "templates/epub/cover.erb"
      copy_file "epub.erb", "templates/epub/page.erb"
      copy_file "toc.erb", "templates/epub/toc.erb"
    end

    def create_empty_directories
      empty_directory "output"
      empty_directory "text"
    end

    def create_git_files
      create_file ".gitignore" do
        "output\n.sass-cache"
      end
      create_file "assets/.gitkeep"
      create_file "assets/fonts/.gitkeep"
      create_file "assets/images/.gitkeep"
      create_file "assets/styles/.gitkeep"
      create_file "output/.gitkeep"
      create_file "text/.gitkeep"
    end
  end
end
