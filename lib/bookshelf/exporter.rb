module Bookshelf
  class Exporter
    def self.run(book_dir, options)
      exporter = new(book_dir, options)
      exporter.export!
    end

    attr_accessor :book_dir
    attr_accessor :options

    def initialize(book_dir, options)
      @book_dir = book_dir
      @options = options
    end

    def ui
      @ui ||= Thor::Base.shell.new
    end

    def export_assets
      FileUtils.mkdir_p("output/assets/fonts")
      FileUtils.mkdir_p("output/assets/images")
      FileUtils.mkdir_p("output/assets/styles")
      FileUtils.cp_r(Dir.glob('assets/fonts/*'), "output/assets/fonts/")
      FileUtils.cp_r(Dir.glob('assets/images/*'), "output/assets/images/")
      ["epub","html"].each do |style_sheet|
        `sass assets/styles/#{style_sheet}.scss output/assets/styles/#{style_sheet}.css`
      end
    end

    def export!
      helper = Bookshelf.root_dir.join("config/helper.rb")
      load(helper) if helper.exist?

      export_assets
      export_pdf = [nil, "pdf"].include?(options[:only])
      export_epub = [nil, "epub"].include?(options[:only])

      exported = []
      exported << Parser::HTML.parse(book_dir)
      exported << Parser::PDF.parse(book_dir) if export_pdf && Dependency.prince?
      exported << Parser::Epub.parse(book_dir) if export_epub

      if exported.all?
        color = :green
        message = options[:auto] ? "exported!" : "** e-book has been exported"
        Notifier.notify(
          :image   => Bookshelf::ROOT.join("templates/ebook.png"),
          :title   => "Bookshelf",
          :message => "Your \"#{config[:title]}\" e-book has been exported!"
        )
      else
        color = :red
        message = options[:auto] ? "could not be exported!" : "** e-book couldn't be exported"
      end

      ui.say message, color
    end

    def config
      Bookshelf.config
    end
  end
end
