module Bookshelf
  class Markdown
    # Supported Markdown libraries
    MARKDOWN_LIBRARIES = %w[Maruku]

    # Retrieve preferred Markdown processor.
    def self.engine
      @engine ||= Object.const_get(MARKDOWN_LIBRARIES.find {|lib| Object.const_defined?(lib)})
    end

    # Convert Markdown to HTML.
    def self.to_html(content)
      engine.new(content).to_html
    end
  end
end
