# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "bookshelf/version"

Gem::Specification.new do |s|
  s.name                  = "bookshelf"
  s.version               = Bookshelf::Version::STRING
  s.platform              = Gem::Platform::RUBY
  s.required_ruby_version = ">= 1.9"
  s.authors               = ["Brad Crawford"]
  s.email                 = ["brad.robert.crawford@gmail.com"]
  s.homepage              = "https://bitbucket.org/bradcrawford/bookshelf"
  s.summary               = "A framework that generates PDF and e-Pub from Markdown, and HTML files."
  s.description           = s.summary
  s.license               = "MIT"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency "activesupport"
  s.add_dependency "nokogiri"
  s.add_dependency "maruku"
  s.add_dependency "i18n"
  s.add_dependency "thor"
  s.add_dependency "notifier"
  s.add_dependency "sass"
  s.add_dependency "rubyzip"

  s.add_development_dependency "rspec"
  s.add_development_dependency "test_notifier"
  s.add_development_dependency "rake"
  s.add_development_dependency "pry"
  s.add_development_dependency "pry-nav"
  s.add_development_dependency "awesome_print"
end
