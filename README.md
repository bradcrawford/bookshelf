# Bookshelf

Bookshelf is a tool for the self-publishing of books. It is inspired by (and heavily based off) the excellent Kitabu gem [https://github.com/fnando/kitabu]().

## Features
* Generate HTML, PDF and ePub files
* Multi book support
* Write using Markdown or plain HTML
* Book layout support
* Custom font support
* SCSS support
* Table of Contents generation

## Installation

To install Bookshelf, you’ll need a working Ruby 1.9+ installation.
If you’re cool with it, just run the following command to install it.

    gem install bookshelf

After installing Bookshelf, run the following command to check your external
dependencies.

    $ bookshelf check

    Prince XML: Converts HTML files into PDF files.
    Installed.

There's no requirements here; just make sure you cleared the correct dependency based
on the formats you want to export to.

## Creating a new Book

    $ bookshelf new my_book

This command creates a directory `my_book` with the following structure:

    my_book
    ├── assets  
    │   ├── fonts
    │   ├── images
    │   └── styles
    │       ├── _fonts.scss
    │       ├── epub.scss
    │       └── html.scss
    ├── text
    ├── config
    │   ├── config.yml
    │   └── helper.rb
    ├── output
    └── templates
        ├── epub
        │   ├── cover.erb
        │   ├── page.erb
        └── html
            └── layout.erb


Update `my_book/config/config.yml` to reflect the books title, author and other attributes.

## Authoring

Now, create the content for your book by creating markdown or html files into the `my_book/text` directory. The order is determined by the numeric prefix assigned to the file.

    my_book
    └── text
        ├── 01_introduction.markdown
        ├── 02_chapter_1.markdown
        ├── 03_chapter_2.markdown
        ├── 04_chapter_3.markdown
        └── ...

Note that you if the number of chapters in your book is likely to exceed 10, you should add an extra 0 to the front of the chapter directory/file names

Note that you can use any format you want at the same time. Just use one of the following extensions: `.html`, `.markdown`, `.mkdn` or `.md`

## Exporting

You'll want to see your progress eventually; it's time for you to generate the book PDF. Just run the command `bookshelf export` and your book will be created in the `output` directory.

Bookshelf can generate a Table of Contents (TOC) based on your `h2` tags.

To print the TOC, you need to print a variable called `toc`, using the eRb tag.

    <%= toc %>

## References

* Markdown: http://daringfireball.net/projects/markdown/syntax

## License

(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
